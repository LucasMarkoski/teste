package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import connection.ConnectionFactory;
import model.bean.Passageiro;

public class PassageiroDAO {
	public void create(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = con.prepareStatement("INSERT INTO passageiro (nome, genero, rg, cpf, endereco, email, telefone) VALUES (?, ?, ?, ?, ?, ?, ?)");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setInt(3, p.getRg());
			stmt.setInt(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setInt(7, p.getTelefone());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar: " + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	public List<Passageiro> read() {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		List<Passageiro> passageiros = new ArrayList<>();
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro");
			rs = stmt.executeQuery();
			while (rs.next()) {
				Passageiro p = new Passageiro();
				p.setIdPassageiro(rs.getInt("id_passageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("genero"));
				p.setRg(rs.getInt("rg"));
				p.setCpf(rs.getInt("cpf"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getInt("telefone"));
				passageiros.add(p);
			}
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao exibir as informa��es do banco de dados: " + e);
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return passageiros;
	}
	public void delete(Passageiro f) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		
		try {
			stmt = con.prepareStatement("DELETE FROM passageiro WHERE id_passageiro = ?");
			stmt.setInt(1, f.getIdPassageiro());
			stmt.executeUpdate();
			
			JOptionPane.showMessageDialog(null, "Passageiro exclu�do com sucesso!");
		} catch (SQLException e ){
			JOptionPane.showMessageDialog(null, "Erro ao excluir: " + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
	public Passageiro read(int id) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		Passageiro p = new Passageiro();
		
		try {
			stmt = con.prepareStatement("SELECT * FROM passageiro WHERE id_passageiro = ? LIMIT 1;");
			stmt.setInt(1, id);
			rs = stmt.executeQuery();
			if (rs != null && rs.next()) {
				p.setIdPassageiro(rs.getInt("id_passageiro"));
				p.setNome(rs.getString("nome"));
				p.setGenero(rs.getString("genero"));
				p.setRg(rs.getInt("rg"));
				p.setCpf(rs.getInt("cpf"));
				p.setEndereco(rs.getString("endereco"));
				p.setEmail(rs.getString("email"));
				p.setTelefone(rs.getInt("telefone"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			ConnectionFactory.closeConnection(con, stmt, rs);
		}
		return p;
	}
	public void update(Passageiro p) {
		Connection con = ConnectionFactory.getConnection();
		PreparedStatement stmt = null;
		try {
			stmt = con.prepareStatement("UPDATE passageiro SET nome = ?, genero = ?, rg = ?, cpf = ?, endereco = ?, email = ?, telefone = ? WHERE id_passageiro = ?");
			stmt.setString(1, p.getNome());
			stmt.setString(2, p.getGenero());
			stmt.setInt(3, p.getRg());
			stmt.setInt(4, p.getCpf());
			stmt.setString(5, p.getEndereco());
			stmt.setString(6, p.getEmail());
			stmt.setInt(7, p.getTelefone());
			stmt.setInt(8, p.getIdPassageiro());
			
			stmt.executeUpdate();
			JOptionPane.showMessageDialog(null, "Atualizado com sucesso!");
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "Erro ao atualizar: " + e);
		} finally {
			ConnectionFactory.closeConnection(con, stmt);
		}
	}
}

