package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class JFListarPassageiros extends JFrame {

	private JPanel contentPane;
	private JTable JTPassageiros;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFListarPassageiros frame = new JFListarPassageiros();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFListarPassageiros() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowActivated(WindowEvent e) {
				readJTable();
			}
		});
		setTitle("Listar passageiros");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 880, 550);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblListarPassageiros = new JLabel("Listar Passageiros");
		lblListarPassageiros.setHorizontalAlignment(SwingConstants.CENTER);
		lblListarPassageiros.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblListarPassageiros.setBounds(10, 11, 844, 24);
		contentPane.add(lblListarPassageiros);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 46, 844, 420);
		contentPane.add(scrollPane);
		
		JTPassageiros = new JTable();
		JTPassageiros.setFont(new Font("Tahoma", Font.PLAIN, 12));
		JTPassageiros.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"ID", "Nome", "Email", "Telefone"
			}
		));
		scrollPane.setViewportView(JTPassageiros);
		
		JButton btnCadastrarPassageiro = new JButton("Cadastrar Passageiro");
		btnCadastrarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFCadastrarPassageiro cp = new JFCadastrarPassageiro();
				cp.setVisible(true);
			}
		});
		btnCadastrarPassageiro.setBounds(85, 477, 174, 23);
		contentPane.add(btnCadastrarPassageiro);
		
		JButton btnAlterarPassageiro = new JButton("Alterar Passageiro");
		btnAlterarPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JTPassageiros.getSelectedRow() != -1) {
					JFAtualizarPassageiro ap = new JFAtualizarPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(),0));
					ap.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
				}
				readJTable();
			}
		});
		btnAlterarPassageiro.setBounds(344, 477, 174, 23);
		contentPane.add(btnAlterarPassageiro);
		
		JButton btnExcluirPassageiro = new JButton("Excluir Passageiro");
		btnExcluirPassageiro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (JTPassageiros.getSelectedRow() != -1) {
					int opcao = JOptionPane.showConfirmDialog(null, "Deseja excluir o passageiro selecionado?", "Exclus�o", JOptionPane.YES_NO_OPTION);
					if (opcao == 0) {
						PassageiroDAO dao = new PassageiroDAO();
						Passageiro f = new Passageiro();
						f.setIdPassageiro((int)JTPassageiros.getValueAt(JTPassageiros.getSelectedRow(), 0));
						dao.delete(f);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Selecione um passageiro!");
				}
				readJTable();
			}
		});
		btnExcluirPassageiro.setBounds(603, 477, 174, 23);
		contentPane.add(btnExcluirPassageiro);
		readJTable();
	}
	public void readJTable() {
		DefaultTableModel modelo = (DefaultTableModel) JTPassageiros.getModel();
		modelo.setNumRows(0);
		PassageiroDAO pdao = new PassageiroDAO();
		for (Passageiro p : pdao.read()) {
			modelo.addRow(new Object[] {
				p.getIdPassageiro(),
				p.getNome(),
				p.getEmail(),
				p.getTelefone()
			});
		}
	}
}

