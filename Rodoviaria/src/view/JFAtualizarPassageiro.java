package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import model.bean.Passageiro;
import model.dao.PassageiroDAO;

import javax.swing.JLabel;
import java.awt.Color;

public class JFAtualizarPassageiro extends JFrame {

	private JPanel contentPane;
	private JTextField txtNome;
	private JTextField txtGenero;
	private JTextField txtEndereco;
	private JTextField txtEmail;
	private static int id;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFAtualizarPassageiro frame = new JFAtualizarPassageiro(id);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFAtualizarPassageiro(int id) {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 390);
		contentPane = new JPanel();
		contentPane.setBackground(Color.GRAY);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		PassageiroDAO pdao = new PassageiroDAO();
		Passageiro p = pdao.read(id);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setBounds(353, 11, 15, 14);
		contentPane.add(lblId);
		
		JLabel lblID = new JLabel("New label");
		lblID.setBounds(378, 11, 46, 14);
		contentPane.add(lblID);
		
		JLabel lblAtualizarPassageiro = new JLabel("Atualizar Passageiro");
		lblAtualizarPassageiro.setHorizontalAlignment(SwingConstants.CENTER);
		lblAtualizarPassageiro.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblAtualizarPassageiro.setBounds(10, 11, 414, 14);
		contentPane.add(lblAtualizarPassageiro);
		
		JLabel lblNome = new JLabel("Nome");
		lblNome.setBounds(10, 36, 46, 14);
		contentPane.add(lblNome);
		
		txtNome = new JTextField();
		txtNome.setBounds(10, 61, 414, 20);
		contentPane.add(txtNome);
		txtNome.setColumns(10);
		
		JLabel lblGenero = new JLabel("G\u00EAnero");
		lblGenero.setBounds(10, 92, 46, 14);
		contentPane.add(lblGenero);
		
		txtGenero = new JTextField();
		txtGenero.setBounds(10, 117, 200, 20);
		contentPane.add(txtGenero);
		txtGenero.setColumns(10);
		
		JLabel lblRG = new JLabel("RG");
		lblRG.setBounds(220, 92, 46, 14);
		contentPane.add(lblRG);
		
		JSpinner spnRG = new JSpinner();
		spnRG.setBounds(220, 117, 204, 20);
		contentPane.add(spnRG);
		
		JLabel lblCPF = new JLabel("CPF");
		lblCPF.setBounds(10, 148, 46, 14);
		contentPane.add(lblCPF);
		
		JSpinner spnCPF = new JSpinner();
		spnCPF.setBounds(10, 173, 200, 20);
		contentPane.add(spnCPF);
		
		JLabel lblTelefone = new JLabel("Telefone");
		lblTelefone.setBounds(220, 148, 46, 14);
		contentPane.add(lblTelefone);
		
		JSpinner spnTelefone = new JSpinner();
		spnTelefone.setBounds(220, 173, 204, 20);
		contentPane.add(spnTelefone);
		
		JLabel lblEndereco = new JLabel("Endere\u00E7o");
		lblEndereco.setBounds(10, 204, 58, 14);
		contentPane.add(lblEndereco);
		
		txtEndereco = new JTextField();
		txtEndereco.setBounds(10, 229, 414, 20);
		contentPane.add(txtEndereco);
		txtEndereco.setColumns(10);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 260, 46, 14);
		contentPane.add(lblEmail);
		
		txtEmail = new JTextField();
		txtEmail.setBounds(10, 285, 414, 20);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		lblID.setText(String.valueOf(p.getIdPassageiro()));
		txtNome.setText(p.getNome());
		txtGenero.setText(p.getGenero());
		spnRG.setValue(p.getRg());
		spnCPF.setValue(p.getCpf());
		spnTelefone.setValue(p.getTelefone());
		txtEndereco.setText(p.getEndereco());
		txtEmail.setText(p.getEmail());
		
		JButton btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Passageiro p = new Passageiro();
				PassageiroDAO dao = new PassageiroDAO();
				
				p.setIdPassageiro(Integer.parseInt(lblID.getText()));
				p.setNome(txtNome.getText());
				p.setGenero(txtGenero.getText());
				p.setRg(Integer.parseInt(spnRG.getValue().toString()));
				p.setCpf(Integer.parseInt(spnCPF.getValue().toString()));
				p.setEndereco(txtEndereco.getText());
				p.setEmail(txtEmail.getText());
				p.setTelefone(Integer.parseInt(spnTelefone.getValue().toString()));
				
				dao.update(p);
				dispose();
			}
		});
		
		btnAlterar.setBounds(39, 316, 100, 23);
		contentPane.add(btnAlterar);
		
		JButton btnLimpar = new JButton("Limpar");
		btnLimpar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtNome.setText(null);
				txtGenero.setText(null);
				spnRG.setValue(0);
				spnCPF.setValue(0);
				spnTelefone.setValue(0);
				txtEndereco.setText(null);
				txtEmail.setText(null);
			}
		});
		btnLimpar.setBounds(178, 316, 89, 23);
		contentPane.add(btnLimpar);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
		btnCancelar.setBounds(306, 316, 89, 23);
		contentPane.add(btnCancelar);
	}
}
